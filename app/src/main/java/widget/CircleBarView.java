package widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.nju.myprogressbar.R;

import Utils.DpOrPxUtils;
import Utils.LinearGradientUtil;

public class CircleBarView extends View {
    private Paint progressPaint;//绘制圆弧的画笔
    private Paint bgPaint;//绘制背景圆弧的画笔
    private Paint textPaint;
    private RectF mRectF;//绘制圆弧的矩形区域
    private float barWidth;//圆弧进度条宽度
    private int defaultSize;//自定义View默认的宽高
    private float sweepAngle;//圆弧经过的角度
    private float progressNum;//可以更新的进度条数值
    private float maxNum;//进度条最大值
    private float progressSweepAngle;//进度条圆弧扫过的角度
    private float startAngle;//背景圆弧的起始角度
    private float curPercent;
    private int progressColor;//进度条圆弧颜色
    private int bgColor;//背景圆弧颜色
    private LinearGradientUtil linearGradient;
    private boolean isGradient=false;
    private int textColor;
    private int textSize;

    private int bottomTextSize;
    private int bottomTextColor;
    private Paint bottomTextPaint;

    private  CircleBarAnim anim;
    private String unit;
    private String description;


    public CircleBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
        anim = new CircleBarAnim();
    }

    private void init(Context context,AttributeSet attrs){

        unit="%";
        description="当前百分比";

        defaultSize = DpOrPxUtils.dip2px(context,100);
        mRectF = new RectF();
        progressPaint = new Paint();
        progressPaint.setStyle(Paint.Style.STROKE);//只描边，不填充
        progressPaint.setAntiAlias(true);//设置抗锯齿
        progressPaint.setStrokeCap(Paint.Cap.ROUND);


        bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.STROKE);//只描边，不填充
        bgPaint.setAntiAlias(true);//设置抗锯齿
        bgPaint.setStrokeCap(Paint.Cap.ROUND);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleBarView);

        progressColor = typedArray.getColor(R.styleable.CircleBarView_progress_color,Color.GREEN);//默认为绿色
        bgColor = typedArray.getColor(R.styleable.CircleBarView_bg_color,Color.GRAY);//默认为灰色
        startAngle = typedArray.getFloat(R.styleable.CircleBarView_start_angle,0);//默认为0
        sweepAngle = typedArray.getFloat(R.styleable.CircleBarView_sweep_angle,360);//默认为360
        barWidth = typedArray.getDimension(R.styleable.CircleBarView_bar_width,DpOrPxUtils.dip2px(context,10));//默认为10dp
        textColor=typedArray.getColor(R.styleable.CircleBarView_text_color,Color.BLACK);
        textSize=(int)typedArray.getDimension(R.styleable.CircleBarView_text_size,DpOrPxUtils.dip2px(context,10));
        bottomTextColor=typedArray.getColor(R.styleable.CircleBarView_bottom_text_color,Color.BLACK);
        bottomTextSize=(int)typedArray.getDimension(R.styleable.CircleBarView_bottom_text_size,DpOrPxUtils.dip2px(context,10));
        unit=typedArray.getString(R.styleable.CircleBarView_unit);
        description=typedArray.getString(R.styleable.CircleBarView_description);
        typedArray.recycle();//typedArray用完之后需要回收，防止内存泄漏


        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(barWidth);

        bgPaint.setColor(bgColor);
        bgPaint.setStrokeWidth(barWidth);

        //设置字画笔
        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setDither(true);
        textPaint.setTextAlign(Paint.Align.CENTER); // 设置文字居中，文字的x坐标要注意
        textPaint.setColor(textColor); // 设置文字颜色
        textPaint.setTextSize(textSize); // 设置要绘制的文字大小
        textPaint.setStrokeWidth(0); // 注意此处一定要重新设置宽度为0,否则绘制的文字会重叠

        //设置顶部文字
        bottomTextPaint = new Paint();
        bottomTextPaint.setAntiAlias(true);
        bottomTextPaint.setDither(true);
        bottomTextPaint.setTextAlign(Paint.Align.CENTER); // 设置文字居中，文字的x坐标要注意
        bottomTextPaint.setColor(bottomTextColor); // 设置文字颜色
        bottomTextPaint.setTextSize(bottomTextSize); // 设置要绘制的文字大小
        bottomTextPaint.setStrokeWidth(0); // 注意此处一定要重新设置宽度为0,否则绘制的文字会重叠

        this.linearGradient= new LinearGradientUtil(Color.YELLOW, Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(mRectF,startAngle,sweepAngle,false,bgPaint);
        canvas.drawArc(mRectF,startAngle,progressSweepAngle,false, progressPaint);
        drawText(canvas,this.getWidth() / 2);
        drawBottomText(canvas,this.getWidth()/2);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = measureSize(defaultSize, heightMeasureSpec);
        int width = measureSize(defaultSize, widthMeasureSpec);
        int min = Math.min(width, height);// 获取View最短边的长度
        setMeasuredDimension(min, min+40);// 强制改View为以最短边为长度的正方形
        if(min >= barWidth*2){//这里简单限制了圆弧的最大宽度
            mRectF.set(barWidth/2,barWidth/2,min-barWidth/2,min-barWidth/2);
        }
    }

    public class CircleBarAnim extends Animation {

        public CircleBarAnim(){
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            curPercent=interpolatedTime * progressNum / maxNum;
            progressSweepAngle = interpolatedTime * sweepAngle * progressNum / maxNum;//这里计算进度条的比例
            if (isGradient) {
                progressPaint.setColor(linearGradient.getColor(curPercent));
                textPaint.setColor(linearGradient.getColor(curPercent));
                bottomTextPaint.setColor(linearGradient.getColor(curPercent));
            }
            postInvalidate();
        }
    }

    //写个方法给外部调用，用来设置动画时间
    public void setProgressNum(float progressNum,float maxNum,int time) {
        anim.setDuration(time);
        this.startAnimation(anim);
        this.progressNum = progressNum;
        this.maxNum=maxNum;
    }

    private int measureSize(int defaultSize,int measureSpec) {
        int result = defaultSize;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else if (specMode == MeasureSpec.AT_MOST) {
            result = Math.min(result, specSize);
        }
        return result;
    }

    private void drawText(Canvas canvas, int center)
    {
        String percent = String.format("%.1f", curPercent*100) + unit;
        Rect bounds = new Rect(); // 文字边框
        textPaint.getTextBounds(percent, 0, percent.length(), bounds); // 获得绘制文字的边界矩形
        Paint.FontMetricsInt fontMetrics = textPaint.getFontMetricsInt(); // 获取绘制Text时的四条线
        int baseline = center + (fontMetrics.bottom - fontMetrics.top) / 2 - fontMetrics.bottom; // 计算文字的基线
        canvas.drawText(percent, center, baseline, textPaint); // 绘制表示进度的文字
    }

    private void drawBottomText(Canvas canvas, int center)
    {
        String percent = String.format("%.1f", curPercent*100) + unit;
        Rect bounds = new Rect(); // 文字边框
        textPaint.getTextBounds(percent, 0, percent.length(), bounds); // 获得绘制文字的边界矩形
        Paint.FontMetricsInt fontMetrics = textPaint.getFontMetricsInt(); // 获取绘制Text时的四条线
        canvas.drawText(description+percent,center,this.getWidth(),bottomTextPaint);
    }

    public void setLinearGradient(int StartColor,int EndColor) {
        this.linearGradient = new LinearGradientUtil(StartColor,EndColor);
        this.isGradient=true;
    }

}
